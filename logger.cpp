#include "logger.h"
#include <QDateTime>
#include <QCoreApplication>
#include <QSettings>
#include <QDir>
#include <QThread>
#include "quazip/quazip.h"
#include "quazip/quazipfile.h"
#include "quazip/quazipdir.h"
#include "smtp/SmtpMime"
#include "smtp/smtpclient.h"
#include <QtConcurrent>
#include <QDebug>
#ifdef QUAZIP_CAN_USE_QTEXTCODEC
#include <QTextCodec>
#else
#include <QStringConverter>
#endif

#include <iostream>
#include <string>
#ifdef Q_OS_WINDOWS
#include "windows.h"
#endif
#ifdef Q_OS_LINUX
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#endif

#ifdef Q_OS_LINUX
int getch() {
    int ch;
    struct termios t_old, t_new;

    tcgetattr(STDIN_FILENO, &t_old);
    t_new = t_old;
    t_new.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &t_new);

    ch = getchar();

    tcsetattr(STDIN_FILENO, TCSANOW, &t_old);
    return ch;
}
#endif

QString getPass(QString prompt, bool show_asterisk=true)
{
    QString password;
    unsigned char ch=0;
#ifdef Q_OS_WINDOWS
    //QTextCodec *codec = QTextCodec::codecForName("cp866");
    auto pr = QByteArray("promt"); //codec->fromUnicode(prompt);
    std::cout << pr.constData() << std::endl;
#endif

#ifdef Q_OS_LINUX
    std::cout << prompt.toUtf8().constData() << std::endl;
    const char BACKSPACE=127;
    const char RETURN=10;
    while((ch=getch())!=RETURN)
#endif
#ifdef Q_OS_WINDOWS
        const char BACKSPACE=8;
    const char RETURN=13;
    DWORD con_mode;
    DWORD dwRead;
    HANDLE hIn=GetStdHandle(STD_INPUT_HANDLE);
    GetConsoleMode( hIn, &con_mode );
    SetConsoleMode( hIn, con_mode & ~(ENABLE_ECHO_INPUT | ENABLE_LINE_INPUT) );
    while(ReadConsoleA( hIn, &ch, 1, &dwRead, NULL) && ch !=RETURN)
#endif
    {
        if(ch==BACKSPACE)
        {
            if(password.length()!=0)
            {
                if(show_asterisk)
                    std::cout << "\b \b";
                password.resize(password.length()-1);
            }
        }
        else
        {
            password += QChar(ch);
            if(show_asterisk)
                std::cout << '*';
        }
    }
#ifdef Q_OS_WINDOWS
    //Возвращаем обратно
    SetConsoleMode( hIn, con_mode);
#endif
    std::cout << std::endl;
    return password;
}


Logger::Logger(QString namePrefix, QObject *parent, bool additional) : QObject(parent)
{
    connect(&logsWatcher, &QFutureWatcher<QString>::finished,
            this, &Logger::logsSendFinished);
    connect(&zipsWatcher, &QFutureWatcher<QString>::finished,
            this, &Logger::zipsSendFinished);
    watchdog = nullptr;
    settingsScope = QSettings::UserScope;
    filePrefix = namePrefix;
    nameFilters << "*.csv" << "*.txt";
    currentDay = QDateTime::currentDateTime().date().day();
    this->additional = additional;
    qint64 now = QDateTime::currentMSecsSinceEpoch();
    QDateTime tmdt;
    tmdt.setDate(QDate::currentDate().addDays(1));
    auto tom = tmdt.toMSecsSinceEpoch();
    QTimer::singleShot(tom-now+1000, this, &Logger::newDay);
}

Logger::~Logger()
{
    if(watchdog)
        delete watchdog;
}

void Logger::addToLog(const QString &message, Logger::MessageType type)
{
    if(type == MessageType::Debug && !debugEnabled)
        return;
    if(!logFile.isOpen())
        reopenFile();
    QString str;
    str = QString("%1;%2;%3;%4;")
              .arg(QDate::currentDate().toString("dd-MM-yyyy"),
                   QTime::currentTime().toString("hh:mm:ss:zzz"))
              .arg(type)
              .arg(type != Logger::FileOnly && type != Logger::Debug
                   ? QString(message).replace(";", ",")
                       : message);
    logFile.write(tr("%1\n").arg(str).toUtf8());
    if(type != FileOnly && type != Debug)
        emit newMessage(str, type);
    logFile.flush();
}

void Logger::addInfo(const QString &str)
{
    addToLog(str, Info);
}

void Logger::addWarning(const QString &str)
{
    addToLog(str, Warning);
}

void Logger::addError(const QString &str)
{
    addToLog(str, Error);
}

void Logger::addToFileOnly(const QString &str)
{
    addToLog(str, FileOnly);
}

void Logger::addDebug(const QString &str)
{
    addToLog(str, Debug);
}

void Logger::saveToFile(const QString &filename, const QByteArray &data)
{
    QDir dir(logDir);
    QString subdir = QDate::currentDate().toString("yyyy/MM/dd");
    if(!dir.mkpath(subdir))
    {
        addError(tr("Невозможно создать подкаталог %1 в каталоге %2"
                    " для сохранения файла"));
        return;
    }
    QFile file(logDir+"/"+subdir+"/"+filename);
    if(file.open(QIODevice::WriteOnly))
    {
        file.write(data);
        file.close();
    }
    else
        addError(tr("Невозможно сохранить файл в поддиректории %1. Ошибка %2")
                 .arg(logDir+"/"+subdir).arg(file.errorString()));
}

void Logger::initialize()
{
    readSettings();
}
void Logger::reopenFile()
{
    if(logFile.isOpen())
        logFile.close();
    logFile.setFileName(QString("%1/%2_%3.csv").arg(logDir).arg(filePrefix)
                        .arg(QDate::currentDate().toString("dd_MM_yyyy")));
    logFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append);
}
void Logger::readSettings()
{
    QSettings settings(settingsScope);
    settings.beginGroup("Log");
    setLogDirectory(settings.value("logDir",
                               #ifdef Q_OS_ANDROID
                                   QStandardPaths::writableLocation(
                                       QStandardPaths::AppDataLocation)+"/logs"
#else
                                   QCoreApplication::applicationDirPath()+"/logs"
#endif
                    ).toString());
    deleteLog = settings.value("logDelete", true).toBool();
    daysToDelete = settings.value("logDeletePeriod", 1).toInt();
    sizeHuge = settings.value("logDeleteSize").toInt();
    deleteHuge = settings.value("logDeleteHuge").toBool();
    fileArchive = settings.value("fileArchive", 30).toInt();
    archiveDelete = settings.value("archiveDelete", 24).toInt();
    lastArchiveDay = settings.value("lastArchiveDay").toDate();
    addLogsToArchive = settings.value("addLogsToArchive", true).toBool();
    senderEmail = settings.value("senderEmail",
                                 DefaultSenderEmail).toString();
    senderPassword = settings.value("senderPassword",
                                    DefaultSenderPassword).toString();
    smtpServer = settings.value("smtpServer").toString();
    smtpPort = settings.value("smtpPort", 465).toInt();
#ifdef RECEPIENT_EMAILS
    developerEmail = settings.value("developerEmail", RECEPIENT_EMAILS).toString();
#else
    developerEmail = settings.value("developerEmail",
                                    "support@secresilent.ru").toString();
#endif
    stationName = settings.value("stationName",
                                 qApp->applicationName()).toString();
#ifdef DEBUG_ENABLED_DEFAULT
    bool defaultDebugEnabled = true;
#else
    bool defaultDebugEnabled = false;
#endif
    debugEnabled = settings.value("debugEnabled", defaultDebugEnabled).toBool();
    settings.endGroup();
    if(!additional && (deleteLog || deleteHuge)) //Активировано хоть одно удаление автоматическое
    {
        if(watchdog == nullptr)
        {
            watchdog = new QTimer(this);
            connect(watchdog, &QTimer::timeout,
                    this, &Logger::logRemoveWatchdog);
        }
        if(!watchdog->isActive())
            watchdog->start(1000*10*60); //10 минут
    }
    else
        if(watchdog)
        {
            watchdog->stop();
            delete watchdog;
            watchdog = nullptr;
        }
}

void Logger::newDay()
{
    if(QDate::currentDate().day() != currentDay) //Если день сменился
    {
        logFile.close();
        currentDay = QDate::currentDate().day();
        if(!additional)
            fileArchiveWorker();
    }
    qint64 now_ms = QDateTime::currentMSecsSinceEpoch();
    auto tom = QDateTime();
    tom.setDate(QDate().currentDate().addDays(1));
    auto tom_ms = tom.toMSecsSinceEpoch();
    QTimer::singleShot(tom_ms-now_ms+1000, this, &Logger::newDay);
}

bool Logger::addToZip(QString filepath, QuaZip *zipp, QString zipDir)
{
    QString filename = filepath.section("/", -1, -1);
    QFile tmp;
    tmp.setFileName(filepath);
    if(tmp.open(QIODevice::ReadOnly))
    {
        QByteArray data = tmp.readAll();
        auto result =  addToZip(data, zipDir+filename, zipp);
        addToFileOnly(tr("Добавили файл %1 к архиву")
                      .arg(filename));
        tmp.close();
        return result;
    }
    else
        addToFileOnly(
                    tr("Ошибка чтения файла %1 "
                       "при архивировании. %2")
                    .arg(filename)
                    .arg(tmp.errorString()));
    return false;
}

bool Logger::addToZip(QByteArray content, QString filename, QuaZip *zipp)
{
    QuaZipFile zipFile(zipp);
    QuaZipNewInfo fileInfo(filename);
    if(zipFile.open(QIODevice::WriteOnly,
                    fileInfo))
    {
        zipFile.write(content);
        zipFile.close();
    }
    else
        addToFileOnly(tr("Ошибка добавления "
                         "в архив файла %1")
                      .arg(filename));
    return true;
}

void Logger::addFilesToZip(QuaZip *zipp, QDate limit, bool archiveOlder, bool deleteOlder)
{
    QDir dir(logDir);
    for(auto year_dir: dir.entryList(QDir::Dirs | //Интересуют только каталоги
                                     QDir::NoDotAndDotDot))
    {
        bool ok;
        int year = year_dir.toInt(&ok);
        if(ok) //Если год успешно преобразован
        {
            QDir yearDir(dir.absolutePath()+"/"+year_dir);
            for(auto month_dir: yearDir.entryList(QDir::Dirs
                                                  | QDir::NoDotAndDotDot))
            {
                int month = month_dir.toInt(&ok);
                if(ok)
                {
                    QDir monthDir(yearDir.absolutePath()+"/"+month_dir);
                    for(auto day_dir: monthDir.entryList(QDir::Dirs
                                                         | QDir::NoDotAndDotDot))
                    {
                        int day = day_dir.toInt(&ok);
                        QDir dayDir(monthDir.absolutePath()+"/"+day_dir);
                        if(ok)
                        {
                            QDate dt(year, month, day);
                            if((archiveOlder && dt <= limit) ||//Если вычисленая дата старше лимита
                                    (!archiveOlder && dt >= limit)) //Если нам надо файлы моложе
                            {
                                addToFileOnly(
                                            tr("Архивируем подкаталог %1/%2/%3")
                                            .arg(year).arg(month).arg(day));
                                int count = 0;
                                for(auto &file: dayDir.entryList(QDir::Files))
                                {
                                    if(addToZip(dayDir.absoluteFilePath(file),
                                                zipp, QString("%1/%2/%3/")
                                                .arg(year_dir,
                                                     month_dir,
                                                     day_dir)))
                                    {
                                        count++;
                                        if(deleteOlder)
                                        { //Удаляем файл
                                            if(!dayDir.remove(file))
                                                addToFileOnly(tr("Ошибка удаления "
                                                             "файла %1")
                                                              .arg(file));
                                        }
                                    }
                                }
                                addToFileOnly(tr("Заархировано %1 файлов.")
                                              .arg(count));
                                if(dayDir.isEmpty())
                                    if(!dayDir.removeRecursively())
                                        addToFileOnly(tr("Ошибка удаления "
                                                         "каталога %1")
                                                      .arg(day_dir));
                            }
                        }
                        else
                            addToFileOnly(tr("При попытке архивации не удалось"
                                             " распознать день каталога %1")
                                          .arg(day_dir));
                    }
                    if(monthDir.isEmpty())
                        if(!monthDir.removeRecursively())
                            addToFileOnly(tr("Ошибка удаления "
                                             "каталога %1")
                                          .arg(month_dir));
                }
                else
                    addToFileOnly(tr("При попытке архивации не удалось "
                                     "распознать месяц каталога %1")
                                  .arg(month_dir));
            }
            if(yearDir.isEmpty())
                if(!yearDir.removeRecursively())
                    addToFileOnly(tr("Ошибка удаления "
                                     "каталога %1")
                                  .arg(year_dir));
        }
        else
            addToFileOnly(tr("При попытке архивации не удалось распознать "
                             "год каталога %1").arg(year_dir));
    }
}

bool Logger::makeZip(int days, QString &zipName,
                     const Logger::LoggerAddons &addons)
{
    auto dt = QDateTime().currentDateTime().toString("dd_MM_yyyy_hh_mm_ss");
    zipName = QString("%3/logs%1_%2.zip")
                  .arg(dt, stationName, zipName.isEmpty() ? logDir : zipName);
    addToFileOnly(
        tr("Архивируем файлы логов из %1 в архив %2").arg(logDir).arg(zipName));
    QuaZip *zipp = new QuaZip(zipName);
    zipp->setUtf8Enabled(true);
    QDir dir(logDir);
    if (zipp->open(QuaZip::mdCreate)) {
        for (auto &addon : addons)
            addToZip(addon.content, addon.name, zipp);
        dir.setNameFilters(nameFilters);
        dir.setSorting(QDir::Time);
        for (int i = 0; i < dir.entryList().size(); i++) {
            QFile file(logDir + "/" + dir.entryList()[i]);
            QFileInfo info(file);
            bool fresh =
                info.lastModified().addDays(days) > QDateTime::currentDateTime();
            if (fresh) {
                addToZip(file.fileName(), zipp);
            }
        }
        addFilesToZip(zipp, QDate::currentDate().addDays(-days), false, false);
        zipp->close();
        if (zipp->getZipError() != UNZ_OK) {
            addToFileOnly(
                tr("Ошибка закрытия файла архива %1").arg(zipp->getZipError()));
            delete zipp;
            return false;
        }
        delete zipp;
    } else {
        delete zipp;
        return false;
    }
    return true;
}

void Logger::fileArchiveWorker(bool force)
{
    QDate limit = QDate::currentDate();
    if(!force)
    {
        //Структура каталогов YYYY/MM/DD
        //Вычисляем дату, старше которой мы должны будем забрать файлы
        if(lastArchiveDay.isValid() &&
                lastArchiveDay.addDays(fileArchive) > QDate::currentDate())
            return;
        addToFileOnly(tr("Последняя архивация была %1, производим архивацию")
                      .arg(lastArchiveDay.toString("dd.MM.yyyy")));
        lastArchiveDay = QDate::currentDate();
        limit = lastArchiveDay.addDays(-fileArchive);
        addToFileOnly(tr("Выполняем архивацию файлов старше %1")
                      .arg(limit.toString("dd.MM.yyyy")));
        QSettings sts;
        sts.setValue("Log/lastArchiveDay", lastArchiveDay);
    }
    QString zipName = QString("%1/%2%3.zip")
            .arg(logDir, QDate::currentDate().toString("yyyyMMdd_archive"),
                 force?QString::number(QDateTime::currentMSecsSinceEpoch()):"");
    QuaZip *zipp = new QuaZip(zipName);
    zipp->setUtf8Enabled(true);
    if(!zipp->open(QuaZip::mdCreate))
    {
        addToFileOnly(tr("Ошибка создания архива %1")
                      .arg(zipName));
        return;
    }
    QDir dir(logDir);
    addFilesToZip(zipp, limit, true, !force);
    if(addLogsToArchive)
    {
        //Добавляем в архив все имеющиеся логи
        for(auto &file : dir.entryList(nameFilters, QDir::Files))
            addToZip(dir.absoluteFilePath(file), zipp);
    }
    //Сохраняем архив на диске
    zipp->close();
    //Очищаем память
    delete zipp;
    //Проверка на устаревшие ахривы
    auto limitRemove = lastArchiveDay.addMonths(-archiveDelete);
    addToFileOnly(tr("Проверяем на устаревшие архивы [до %1]")
                  .arg(limitRemove.toString("dd.MM.yyyy")));
    int count  = 0;
    for(auto &zip: dir.entryList(QStringList() << "*_archive.zip"))
    {//"yyyyMMdd_archive.zip")
        auto dt = QDate::fromString(zip, "yyyyMMdd_archive.zip");
        if(dt <= limitRemove)
        {
            count++;
            addToFileOnly(tr("Удаляем устаревший архив %1").arg(zip));
            if(!dir.remove(zip))
                addToFileOnly(tr("Ошибка удаления архива %1")
                              .arg(zip));
        }
    }
    addToFileOnly(tr("Удалено %1 устаревших архивов").arg(count));
}

void Logger::sendLogsThreaded(QString comment, int days, QString recepients,
                              LoggerAddons addons)
{
    QFuture<QString> logsResult =
            QtConcurrent::run([this, comment,days, recepients, addons]()
    {
        return sendLogsWorker(comment,days, recepients, addons);
    });
    logsWatcher.setFuture(logsResult);
}

void Logger::sendZipsThreaded(QString recepients)
{
    QFuture<QString> zipsResult =
            QtConcurrent::run([this,recepients]()
    {
        return sendZipsWorker(recepients);
    });
    zipsWatcher.setFuture(zipsResult);
}

void Logger::makeZipsThreaded(bool force)
{
    auto res = QtConcurrent::run([this, force](){fileArchiveWorker(force);});
}

void Logger::saveZip(int days, const QString &path)
{
    saveLogs(path, days);
}

void Logger::changeSettingsConsole()
{
    QSettings settings(settingsScope);
    if(!settings.isWritable())
    {
        std::cout << "Cannot write settings. "
"Please run with administrator privilegies." << std::endl;
        return;
    }
    QString _senderEmail = senderEmail, _senderPassword = senderPassword,
            _developerEmail = developerEmail, _stationName = stationName,
            _logDir = logDir;
    bool  _addLogsToArchive = addLogsToArchive, save = false;
    int _daysToDelete = daysToDelete,
            _fileArchive = fileArchive, _archiveDelete = archiveDelete;
    _logDir = getInput(tr("Задайте каталог для хранения файлов"), logDir,
                       InputString).toString();
    _daysToDelete = getInput(tr("Задайте количество дней, через которые файлы "
"логов должны автоматически удаляться"), daysToDelete, InputInt).toInt();
    _fileArchive = getInput(tr("Задайте количество дней, через которые логи "
"и служебные файлы должны автоматически архивироваться"),
                            fileArchive, InputInt).toInt();
    _archiveDelete = getInput(tr("Задайте количество месяцев, через которые "
"архивы должны автоматически удаляться"),
                              archiveDelete, InputInt).toInt();
    _senderEmail = getInput(tr("Задайте email отправителя (служебный)"),
                            senderEmail, InputString).toString();
    _senderPassword = getInput(tr("Задайте пароль отправителя (служебный)"),
                               senderPassword, InputString, true).toString();
    _developerEmail = getInput(tr("Задайте email получателя (можно несколько,"
" через точку с запятой)"), developerEmail, InputString).toString();
    _stationName = getInput(tr("Задайте псевдоним для указания отправителя")
                            , stationName, InputString).toString();
    save = getInput(tr("Подтвержите сохранение настроек (y/n)"), QVariant("y")
                    ,InputString).toString() == "y";
    if(save)
    {
        settings.beginGroup("Log");
        settings.value("logDir", _logDir);
        setLogDirectory(_logDir);
        settings.setValue("logDeletePeriod", _daysToDelete);
        settings.setValue("fileArchive", _fileArchive);
        settings.setValue("archiveDelete", _archiveDelete);
        settings.setValue("addLogsToArchive", _addLogsToArchive);
        settings.setValue("senderEmail",_senderEmail);
        settings.setValue("senderPassword", _senderPassword);
        settings.setValue("developerEmail", _developerEmail);
        settings.setValue("stationName", _stationName);
        settings.endGroup();
    }
}

void Logger::deleteLogger()
{
    emit destroy();
}

QString Logger::sendZipsWorker(QString recepient)
{
    auto mailDomain = senderEmail.section("@", 1, 1);
    auto smtpAddr = mailDomain.isEmpty()?"smtp.yandex.ru":
                                         QString("smtp.%1").arg(mailDomain);
    QDir dir(logDir);
    if(dir.entryList(QStringList() << "*.zip", QDir::Files).isEmpty())
    {
        emit sendZipsFinished(tr("Нет архивов для отправки!"));
        return tr("Нет архивов для отправки!");
    }
    SmtpClient smtp(smtpServer.isEmpty() ? smtpAddr : smtpServer, smtpPort,
                    smtpPort == 465
                        ? SmtpClient::ConnectionType::SslConnection
                                    : SmtpClient::ConnectionType::TcpConnection);

    connect(&smtp, &SmtpClient::debugMessage, this, &Logger::addDebug);
    if(!senderPassword.isEmpty()) {
        smtp.setUser(senderEmail);
        smtp.setPassword(senderPassword);
    }
    else
        smtp.setAuthMethod(SmtpClient::AutoNone);
    MimeMessage message;
    message.setSender(new EmailAddress(senderEmail, stationName));
    if(recepient.isEmpty())
        recepient = developerEmail;
    QStringList rec = recepient.split(";", Qt::SkipEmptyParts);
    if(rec.isEmpty())
    {
        return tr("Не заданы получатели!");
    }
    for(auto &r : rec)
        message.addRecipient(new EmailAddress(r, "Разработчик"));
    auto dt = QDateTime().currentDateTime()
            .toString("dd_MM_yyyy_hh_mm_ss");
    message.setSubject(tr("Отправка архивов %1 от %2")
                       .arg(stationName)
                       .arg(dt));
    MimeText text;
    text.setText(tr("Добрый день! Во вложениях вы найдёте архивированные "
                    "данные и логи программы\n"));
    message.addPart(&text);
    QSettings settings;
    for(auto zips: dir.entryList(QStringList() << "*.zip", QDir::Files))
        message.addPart(new MimeAttachment(new QFile(dir.absoluteFilePath(zips))));
    // Now we can send the mail
    QString error, smtpErr;
    if(smtp.connectToHost(smtpErr))
        if(smtp.login(smtpErr))
            if(smtp.sendMail(message, smtpErr))
            {
                error = QString();
            }
            else
            {

                error = tr("Неизвестная ошибка при отправке логов. %1")
                        .arg(smtpErr);
            }
        else
        {

            error = tr("Ошибка входа при отправке письма. "
"Проверьте правильность логина и пароля отправителя. %1").arg(smtpErr);
        }
    else
    {

        error = tr("Ошибка при подключении к серверу почты %1. %2")
                .arg(smtpAddr, smtpErr);
    }
    smtp.quit();
    return error;
}
QString Logger::sendLogsWorker(QString comment, int days,
                               QString recepient, Logger::LoggerAddons addons)
{
    auto mailDomain = senderEmail.section("@", 1, 1);
    auto smtpAddr = mailDomain.isEmpty()?"smtp.yandex.ru":
                                         QString("smtp.%1").arg(mailDomain);
    SmtpClient smtp(smtpServer.isEmpty() ? smtpAddr : smtpServer, smtpPort,
                    smtpPort == 465
                        ? SmtpClient::ConnectionType::SslConnection
                        : SmtpClient::ConnectionType::TcpConnection);
    emit addDebug(tr("Подключение к серверу почты %1:%2")
                      .arg(smtp.getHost())
                      .arg(smtp.getPort()));
    connect(&smtp, &SmtpClient::debugMessage, this, &Logger::addDebug);
    if(!senderPassword.isEmpty()) {
        smtp.setUser(senderEmail);
        smtp.setPassword(senderPassword);
    }
    else
        smtp.setAuthMethod(SmtpClient::AutoNone);
    MimeMessage message;
    message.setSender(new EmailAddress(senderEmail, stationName));
    if(recepient.isEmpty())
        recepient = developerEmail;
    QStringList rec = recepient.split(";", Qt::SkipEmptyParts);
    if(rec.isEmpty())
    {
        return tr("Не заданы получатели!");
    }
    for(auto &r : rec)
        message.addRecipient(new EmailAddress(r, "Разработчик"));
    auto dt = QDateTime().currentDateTime()
            .toString("dd_MM_yyyy_hh_mm_ss");
    message.setSubject(tr("Отправка логов %1 за %2")
                       .arg(stationName)
                       .arg(dt));
    MimeText text;
    text.setText(comment.isEmpty()?tr("Добрый день! В приложении вы найдёте "
                                      "логи программы\n"):comment);
    message.addPart(&text);
    QString zipName;
    addToFileOnly(tr("Архивируем файлы логов из %1 в архив %2")
                  .arg(logDir).arg(zipName));
    if(makeZip(days, zipName, addons))
        message.addPart(new MimeAttachment(new QFile(zipName)));
    else
        return tr("Невозможно создать архив с логами");
    // Now we can send the mail
    QString error, smtpErr;
    if(smtp.connectToHost(smtpErr))
        if(smtp.login(smtpErr))
            if(smtp.sendMail(message, smtpErr))
                error = QString();
            else
            {
                error = tr("Неизвестная ошибка при "
"отправке логов. %1").arg(smtpErr);
            }
        else
        {
            error = tr("Ошибка входа при отправке письма. %1").arg(smtpErr);
        }
    else
    {
        error = tr("Ошибка при подключении к серверу почты %1^%3. %2")
                    .arg(smtp.getHost(), smtpErr).arg(smtp.getPort());
    }
    //Удаляем файл архива
    QDir dir(logDir);
    dir.remove(zipName);
    smtp.quit();
    return error;
}
void Logger::setLogDirectory(QString dir)
{
    logDir = dir;
    if(!additional)
    {
        QString addition;
        Logger::MessageType mtype;
        if(!QDir(dir).exists())
        {
            addition = tr("Каталог лога %1 не существовал! "
                    "Он был создан автоматически!").arg(dir);
            mtype = Logger::Warning;
            if(!QDir().mkpath(dir))
            {
                addition = (tr("Ошибка создания каталога %1!").arg(dir));
                mtype = Logger::Error;
            }
        }
        if(!addition.isEmpty())
            addToLog(addition, mtype);
    }
    reopenFile();
}

void Logger::setSettingsScope(QSettings::Scope sc)
{
    settingsScope = sc;
}

QString Logger::logDirectory() const
{
    return logDir;
}

Logger *Logger::createLog(QString namePrefix, bool main, QObject *parent,
                          LoggerClient *client)
{
    qRegisterMetaType<Logger::MessageType>("Logger::MessageType");
    Logger  *log = new Logger(namePrefix, nullptr, !main);
    QThread *thrd = new QThread();
    log->moveToThread(thrd);
    if(!main && parent)
    {
        connect(parent, &QObject::destroyed,
                thrd, &QThread::quit);
        connect(log, &QObject::destroyed,
                thrd, &QThread::quit);
    }
    else
        connect(qApp, &QCoreApplication::aboutToQuit,
                thrd, &QThread::quit);
    connect(log, &Logger::destroy,
            thrd, &QThread::quit);
    connect(thrd, &QThread::started,
            log, &Logger::initialize);
    connect(thrd, &QThread::finished,
            log, &Logger::deleteLater);
    connect(thrd, &QThread::finished,
            thrd, &QThread::deleteLater);
    log->connectClient(client);
    thrd->start();
    return log;
}

QString Logger::filenamePrefix()
{
    return filePrefix;
}

QVariant Logger::getInput(QString prompt, QVariant defaultValue,
                          Logger::InputType type, bool password)
{
    QString input;
    if(password)
        input = getPass(prompt);
    else
    {
        prompt = QString("%1[%2]:").arg(prompt, defaultValue.toString());
        auto pr = prompt.toUtf8();
#ifdef Q_OS_WINDOWS
        //QTextCodec *codec = QTextCodec::codecForName("cp866");
        //pr = codec->fromUnicode(prompt);
#endif
        std::string line;
        std::cout << pr.constData() << std::endl;
        std::getline(std::cin, line);
        input =QString::fromStdString(line);
    }
    if(input.isEmpty())
        return defaultValue;
    if(type == InputInt)
    {
        bool ok;
        auto _int = input.toInt(&ok);
        if(ok)
            return QVariant(_int);
        else
            return defaultValue;
    }
    return QVariant(input);
}

bool Logger::sendLogs(QString comment, int days,
                      QString recepient, QString &error)
{
    error = sendLogsWorker(comment, days, recepient);
    return error.isEmpty();
}

bool Logger::sendZips(QString recepients, QString &error)
{
    error = sendZipsWorker(recepients);
    return error.isEmpty();
}

bool Logger::saveLogs(const QString &path, int days)
{
    QString zipPath = path;
    if(makeZip(days, zipPath)) {
        addInfo(tr("Архив логов за %1 дней успешно сохранён в %2")
                    .arg(days)
                    .arg(path));
        return true;
    }
    else {
        addWarning(
            tr("Не удалось сохранить архив с логами за %1 дней в каталоге %2")
                .arg(days)
                .arg(path));
    }
    return false;
}

void Logger::connectClient(LoggerClient *client)
{
    if(client)
    {
        connect(client, &LoggerClient::dataToLog,
                this, &Logger::addToFileOnly);
        connect(client, &LoggerClient::info,
                this, &Logger::addInfo);
        connect(client, &LoggerClient::warning,
                this, &Logger::addWarning);
        connect(client, &LoggerClient::error,
                this, &Logger::addError);
        connect(client, &LoggerClient::saveToFile,
                this, &Logger::saveToFile);
        connect(client, &LoggerClient::debugMessage,
                this, &Logger::addDebug);
    }
}
void Logger::logRemoveWatchdog()
{
    QDir dir(logDir);
    dir.setNameFilters(QStringList() << "*.csv" << "*.txt" << "*.json");
    for(auto &info: dir.entryInfoList())
    {
        if(deleteLog)
        {
            QDate date = info.birthTime().date();
            if(date.isValid() && date.addDays(daysToDelete) < QDate::currentDate())
                dir.remove(info.fileName());
        }
        if(deleteHuge)
        {
            auto sizeMb = info.size()/1024/1024;
            if(sizeMb > sizeHuge) //Если размера файла превышает ограничение
            {
                dir.remove(info.fileName());
            }
        }
    }
}

void Logger::logsSendFinished()
{
    if(logsWatcher.isFinished())
        emit sendLogsFinished(logsWatcher.result());
}

void Logger::zipsSendFinished()
{
    if(zipsWatcher.isFinished())
        emit sendLogsFinished(zipsWatcher.result());
}
