#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <QFile>
#include <QTimer>
#include <QCoreApplication>
#include <QDate>
#include <quazip/quazip.h>
#include <smtp/SmtpMime>
#include <QSettings>
#include <QFutureWatcher>
#include "loggerclient.h"


class LOGGERSHARED_EXPORT Logger : public QObject
{
    Q_OBJECT
public:
    static constexpr char DefaultSenderEmail[] = "loggerwidget@yandex.ru";
    static constexpr char DefaultSenderPassword[] = "gtkrhwftrmbuektb";
    struct LoggerAddon
    {
        QByteArray content;
        QString name;
        LoggerAddon(QByteArray c, QString n)
        {
            content = c;
            name = n;
        }
        LoggerAddon()
        {
            content = QByteArray();
            name = QString();
        }
    };
    typedef QList<Logger::LoggerAddon> LoggerAddons;
    explicit Logger(QString namePrefix = QCoreApplication::applicationName(),
                    QObject *parent = nullptr, bool config = false);
    ~Logger();
    enum MessageType {Debug = 0, Info = 1, Warning, Error, FileOnly};
    Q_ENUM(MessageType)
    void setLogDirectory(QString dir);
    void setSettingsScope(QSettings::Scope sc);
    QString logDirectory() const;
    QString getDeveloperEmail() { return developerEmail;};
    static Logger* createLog(QString namePrefix
                             = QCoreApplication::applicationName(),
                             bool main = true, QObject *parent = nullptr,
                             LoggerClient *client = nullptr);
    QString filenamePrefix();
    enum InputType {InputBool, InputInt, InputString};
    static QVariant    getInput(QString prompt, QVariant defaultValue,
                         Logger::InputType type, bool password = false);
    bool    sendLogs(QString comment, int days, QString recepient, QString &error);
    bool    sendZips(QString recepients, QString &error);
    bool    saveLogs(const QString &path, int days);
    void    connectClient(LoggerClient *client);
signals:
    void    newMessage(QString, Logger::MessageType);
    void    sendLogsFinished(QString error = QString());
    void    sendZipsFinished(QString error = QString());
    void    destroy();
public slots:
    //! Добавление сообщения в лог
    void    addToLog(const QString &, Logger::MessageType type);
    //! Добавление информационного сообщения в лог
    void    addInfo(const QString&);
    //! Добавление предупреждения в лог
    void    addWarning(const QString&);
    //! Добавление ошибки в лог
    void    addError(const QString&);
    //! Добавление сообщения только в файловый лог
    void    addToFileOnly(const QString &);
    //! Добавленіе сообщения только в файловый лог если включен отладочный уровень
    void    addDebug(const QString &);
    //! Сохранение пользовательского файла в поддиректорию в логах
    void    saveToFile(const QString &filename, const QByteArray &data);
    void    initialize();
    void    sendLogsThreaded(QString comment, int days,
                           QString recepients = QString(),
                     Logger::LoggerAddons addons =
            LoggerAddons());
    void    sendZipsThreaded(QString recepients = QString());
    void    makeZipsThreaded(bool force = true);
    void    saveZip(int days, const QString &path);
    void    changeSettingsConsole();
    void    deleteLogger();
protected slots:
    QString    sendZipsWorker(QString recepients);
    QString    sendLogsWorker(QString comment, int days,
                           QString recepient, Logger::LoggerAddons addons =
            LoggerAddons());
    void    fileArchiveWorker(bool force = false);
    void    logRemoveWatchdog();
    void    logsSendFinished();
    void    zipsSendFinished();
    void    reopenFile();
    void    readSettings();
    void    newDay();
    bool    addToZip(QString filepath, QuaZip *zipp, QString zipDir = QString());
    bool    addToZip(QByteArray content, QString filename, QuaZip *zipp);
    void    addFilesToZip(QuaZip *zipp, QDate limit, bool archiveOlder = true,
                          bool deleteOlder = false);
    //!
    //! \brief makeZip Функция сохраняет файлы логов в архив за заданное
    //! количество дней
    //! \param days Количество дней для архивирования
    //! \param zippName Вход - каталог для сохранения архива (если пусто,
    //! то каталогом будет текущий каталог хранения логов), после выполнения
    //! функции будет содержать полный путь к архиву (имя архива генерируется
    //! автоматически на основании текущей даты и имени станции)
    //! \param addons Дополнительные файлы, добавляемые в архив
    //! (настройки, скриншоты)
    //! \return true - если архив успешно сохранён, false - если произошла
    //! ошибка создания архива. В случае успеха пользователь должен сам удалить
    //! файл архива, когда он больше не требуется
    //!
    bool makeZip(int days, QString &zippName,
                 const Logger::LoggerAddons &addons = {});

protected:
    QDate   lastArchiveDay;
    QString filePrefix;
    QString logDir;
    QFile   logFile;
    QStringList nameFilters;
    QString senderEmail, senderPassword, developerEmail, stationName,
        smtpServer;
    int     smtpPort;
    bool    additional;
    bool    deleteLog;
    bool    deleteHuge;
    bool    addLogsToArchive;
    int     sizeHuge;
    int     fileArchive;
    int     archiveDelete;
    int     daysToDelete;
    QTimer  *watchdog;
    int     currentDay;
    QSettings::Scope    settingsScope;
    QFutureWatcher<QString> logsWatcher, zipsWatcher;
#ifdef DEBUG_ENABLED_DEFAULT
    bool    debugEnabled = true;
#else
    bool    debugEnabled = false;
#endif
};
#endif // LOGGER_H
