#ifndef LOGGERCLIENT_H
#define LOGGERCLIENT_H

#include "logger_global.h"
#include <QObject>

class LOGGERSHARED_EXPORT LoggerClient: public QObject
{
    Q_OBJECT
public:
    explicit LoggerClient(QObject *parent = nullptr) : QObject(parent)
    {};
signals:
    //! Сохранение пользовательского файла в поддиректорию в логах
    void    saveToFile(const QString &filename, const QByteArray &data) const;
    void    error(const QString &) const;
    void    warning(const QString &) const;
    void    info(const QString &) const;
    void    dataToLog(const QString &) const;
    void    debugMessage(const QString &) const;
};

#endif // LOGGERCLIENT_H
