#include "memoryusagelogger.h"
#ifdef Q_OS_WINDOWS
#include <windows.h>
#include <stdio.h>
#include <psapi.h>
#endif
#include <QThread>
#include <QTimer>
// To ensure correct resolution of symbols, add Psapi.lib to TARGETLIBS
// and compile with -DPSAPI_VERSION=1
#include "logger.h"

QThread* MemoryUsageLogger::monitorThread = nullptr;

void MemoryUsageLogger::writeMemoryUsage(unsigned long processId)
{
#ifdef Q_OS_WINDOWS
    HANDLE hProcess;
    PROCESS_MEMORY_COUNTERS pmc;
    hProcess = OpenProcess(  PROCESS_QUERY_INFORMATION |
                                    PROCESS_VM_READ,
                                    FALSE, processId );
    if (NULL == hProcess)
        return;

    if ( GetProcessMemoryInfo( hProcess, &pmc, sizeof(pmc)) )
    {
        auto wss = pmc.WorkingSetSize/1024/1024.0;
        auto wssp = pmc.PeakWorkingSetSize/1024/1024.0;
        auto pf = pmc.PagefileUsage/1024/1024.0;
        auto pfp = pmc.PeakPagefileUsage/1024/1024.0;
        emit dataToLog(tr("ALL: %2/%3 Mb(%4),"
                          " RAM: %5/%6 Mb,"
                          " SWAP: %7/%8 Mb")
                       .arg(wss+pf, 0, 'f', 1)
                       .arg(wssp+pfp, 0, 'f', 1)
                       .arg(wss+pf-prevTotal, 0, 'f', 1)
                       .arg(wss, 0, 'f', 1)
                       .arg(wssp, 0, 'f', 1)
                       .arg(pf, 0, 'f', 1)
                       .arg(pfp, 0, 'f', 1));
        prevTotal = wss+pf;
    }
    else
        emit error(tr("Невозможно получить сведения для процесса: %1")
                   .arg(processId));
    CloseHandle( hProcess );
#endif
}

void MemoryUsageLogger::getMemoryUsage()
{
#ifdef Q_OS_WINDOWS
    /*DWORD aProcesses[1024], cbNeeded, cProcesses;
    unsigned int i;
    if ( !EnumProcesses( aProcesses, sizeof(aProcesses), &cbNeeded ) )
    {
        emit error(tr("Невозможно получить сведения о процессах"));
        return;
    }
    cProcesses = cbNeeded / sizeof(DWORD);
    emit info(tr("Используется %1 процессов").arg(cProcesses));
    for ( i = 0; i < cProcesses; i++ )
    {
        writeMemoryUsage(aProcesses[i]);
    }*/
    writeMemoryUsage(GetCurrentProcessId());
#endif
}

void MemoryUsageLogger::startMonitoring(uint period)
{
    if(!monitorThread)
    {
        monitorThread = new QThread();
        auto mul = new MemoryUsageLogger();
        auto timer = new QTimer();
        mul->moveToThread(monitorThread);
        timer->moveToThread(monitorThread);
        //Удаление
        connect(monitorThread, &QThread::finished,
                timer, &QObject::deleteLater);
        connect(monitorThread, &QThread::finished,
                mul, &QObject::deleteLater);
        connect(monitorThread, &QThread::finished,
                monitorThread, &QObject::deleteLater);
        //Запуск
        connect(monitorThread, &QThread::started,
                timer, QOverload<>::of(&QTimer::start));
        //Работа
        connect(timer, &QTimer::timeout,
                mul, &MemoryUsageLogger::getMemoryUsage);

        timer->setInterval(period);
        monitorThread->start();
    }
}

void MemoryUsageLogger::stopMonitoring()
{
    if(monitorThread)
    {
        monitorThread->quit();
        monitorThread = nullptr;
    }
}

MemoryUsageLogger::MemoryUsageLogger(QObject *parent)
    : LoggerClient{parent}
{
    Logger::createLog("MemoryUsage", false, this, this);
}
