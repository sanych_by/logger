#ifndef MEMORYUSAGELOGGER_H
#define MEMORYUSAGELOGGER_H

#include "loggerclient.h"
#include <QThread>
#include "logger_global.h"

class LOGGERSHARED_EXPORT MemoryUsageLogger : public LoggerClient
{
    Q_OBJECT
public:
    static void startMonitoring(uint period = 1000);
    static void stopMonitoring();
    explicit MemoryUsageLogger(QObject *parent = nullptr);
protected slots:
    void    writeMemoryUsage(unsigned long processId);
    void    getMemoryUsage();
protected:
    static QThread *monitorThread;
    double prevTotal = 0.0;
};

#endif // MEMORYUSAGELOGGER_H
